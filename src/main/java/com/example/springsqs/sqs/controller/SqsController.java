package com.example.springsqs.sqs.controller;

import com.example.springsqs.sqs.dto.GoodDto;
import com.example.springsqs.sqs.service.SqsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RequiredArgsConstructor
@RestController
public class SqsController {

    @Autowired
    private SqsService sqsService;

    @PostMapping(value = "/message", consumes = MediaType.ALL_VALUE)
    public HashMap<String, Object> insertGoodFromSbn(@RequestBody GoodDto Good) throws Exception {

        sqsService.sendSqsSingleMsg(Good);

        HashMap<String, Object> rtnObj = new HashMap<>();
        rtnObj.put("msg","OK");

        return rtnObj;
    }

    @PostMapping(value = "/message/list", consumes = MediaType.ALL_VALUE)
    public HashMap<String, Object> insertGoodFromSbn(@RequestBody List<GoodDto> Goods) throws Exception {

        sqsService.sendSqsMsg(Goods);

        HashMap<String, Object> rtnObj = new HashMap<>();
        rtnObj.put("msg","OK");

        return rtnObj;
    }
}
