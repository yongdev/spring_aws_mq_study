package com.example.springsqs.sqs.aws;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.aws.messaging.listener.Acknowledgment;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor
public class AwsSqsListener {
    @SqsListener(value = "${cloud.aws.sqs.queue.name}"
            , deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS
            /*
                ALWAYS 무조건 삭제
                NEVER 절대 삭제 안함 => ack으로 삭제요청 보냄
                ON_SUCCESS 에러가 나지 않으면 삭제
            * */
    )
    public void listen(@Payload String info, @Headers Map<String, String> headers
//            , Acknowledgment ack
    ) throws InterruptedException {
        log.info("-------------------------------------start SqsListener");
        log.info("-------------------------------------info {}", info);
        log.info("-------------------------------------SenderId {}", headers.get("SenderId"));
        log.info("-------------------------------------lookupDestination {}", headers.get("lookupDestination"));
        log.info("-------------------------------------MessageId {}", headers.get("MessageId"));

        //수신후 삭제처리
//        ack.acknowledge();
        log.info("성공 후 삭제됨");
//        try {
//            Thread.sleep(300000);
//            ack.acknowledge();
//            log.info("message delete");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}
