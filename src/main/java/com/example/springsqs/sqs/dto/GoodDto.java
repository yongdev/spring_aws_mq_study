package com.example.springsqs.sqs.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GoodDto {

    @NotEmpty(message = "상품번호는필수")
    private String goodId;

    private String goodNm;

    private int saleAmt;

    private int normalAmt;

    public String getEtyToStr() {
        Map<String ,Object> entity = new HashMap<>();
        entity.put("goodId", this.getGoodId());
        entity.put("goodNm", this.getGoodNm());
        entity.put("saleAmt", this.getSaleAmt());
        entity.put("normalAmt", this.getNormalAmt());

        return String.valueOf(entity);
    }
}
