package com.example.springsqs.sqs.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class GoodMst {

    @Id
    private Long goodId;

    private String goodNm;

    private int saleAmt;

    private int normalAmt;

    private Date regDt;

    private Date modDt;

}
