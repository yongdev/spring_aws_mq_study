package com.example.springsqs.sqs.service;

import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.*;
import com.example.springsqs.sqs.dto.GoodDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.lang.UnsupportedOperationException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SqsService {


//    @Value("${cloud.aws.sqs.queue.url}")
//    private String queueUrl;

    private AmazonSQS sqs;

    public SqsService() {
        this.sqs = AmazonSQSClientBuilder.defaultClient();
    }

    private static final String QUEUE_NAME = "cozy_sqs_test";
    public static final String GOOD_SQS_RESULT_SUCCESS = "SUCCESS";
    public static final String GOOD_SQS_RESULT_FAILED = "FAILED";

    public String sendSqsSingleMsg(GoodDto goodDto) throws Exception {
        try {

            // create queue
            sqs.createQueue(QUEUE_NAME);

            // get queue url
            String queryUrl = sqs.getQueueUrl(QUEUE_NAME).getQueueUrl();

            SendMessageRequest send_msg_request = new SendMessageRequest()
                    .withQueueUrl(queryUrl)
                    .withMessageBody(goodDto.getGoodNm());
                    //.withDelaySeconds(5);
            log.info("send Msg ===>    "  +goodDto.getGoodNm());
            SendMessageResult sendMessageResult = sqs.sendMessage(send_msg_request);
            log.info("MessageId   ===>  "  + sendMessageResult.getMessageId());
            log.info("SequenceNumber   ===>  "  + sendMessageResult.getSequenceNumber());
            if (Objects.isNull(sendMessageResult.getMessageId()) || "".equals(sendMessageResult.getMessageId())) {
                throw new NullPointerException(GOOD_SQS_RESULT_FAILED);
            }

            return GOOD_SQS_RESULT_SUCCESS;

        }catch (UnsupportedOperationException | InvalidMessageContentsException | NullPointerException e) {
            throw new Exception("error on sending one Order");
        }
    }

    public String sendSqsMsg(List<GoodDto> goodDtos) throws Exception {
        try {

            // create queue
            sqs.createQueue(QUEUE_NAME);

            // get queue url
            String queryUrl = sqs.getQueueUrl(QUEUE_NAME).getQueueUrl();

            SendMessageBatchRequest sendMessageBatchRequest  = new SendMessageBatchRequest  ()
                    .withQueueUrl(queryUrl)
                    .withEntries(this.setGoods(goodDtos));

//            log.info("setGood =======>     "  +this.setGoods(goodDtos));

            SendMessageBatchResult sendMessageBatchResult = sqs.sendMessageBatch(sendMessageBatchRequest);

            // get response
            if (!sendMessageBatchResult.getFailed().isEmpty()
                    || sendMessageBatchResult.getSuccessful().isEmpty()
                    || sendMessageBatchResult.getSuccessful().size() < goodDtos.size())  {
                throw new NullPointerException(GOOD_SQS_RESULT_FAILED);
            }

            return GOOD_SQS_RESULT_SUCCESS;

        }catch (UnsupportedOperationException | InvalidMessageContentsException | NullPointerException e) {
            throw new Exception("error on sending one Order");
        }
    }

    private List<SendMessageBatchRequestEntry> setGoods(List<GoodDto> goods) {
        log.info("goods   = >   " + goods);
        return goods.stream()
                .map(good -> new SendMessageBatchRequestEntry(good.getGoodId(), good.getEtyToStr()))
                .collect(Collectors.toList());
    }

}
