# AWS Lambda + SQS  🙋


## SQS (Simple Queue Service)란 ✔

> - 서버들끼리 사용할 수 있는 **MQ(Message Queue)** 를 제공하는 서비스
> - 처리해야할 작업을 나중에 처리하거나, 다른 시스템이 처리할 수 있도록 하기 위한 **비동기 메시징 서비스**


## SQS 와 같은 MQ 서비스를 사용하는 이유 ✔

> - 서비스가 커질수록 여러 서버에서 기능들을 처리하게 되면서, 서버끼리 주고 받는 메시지를 유실되지 않고 정확하게 처리하는 것이 중요해짐 


## MQ 의 종류  ✔

- Kafka
- RabbitMQ
- Redis Pub/Sub
- AWS SQS
- AWS SNS (Simple Notification Service)
- AWS MSK (Amazon Managed Streaming for Apache Kafka)
- AWS Kinesis (Amazon Kinesis Data Streams)

## MQ를 적용할 수 있는 상황 및 사례 ✔

>좋은예시
>[천만 명의 사용자에게 1분 내로 알림 보내기 (병렬프로세스의 최적화) - 👨‍💻꿈꾸는 태태태의 공간 (taetaetae.github.io)](https://taetaetae.github.io/2019/01/02/faster-parallel-processes/)

### 1. 상황

- 작업에 대한 응답이 필요하지 않는 경우
- 동시에 많은 요청 
- 어플리케이션 간 또는 내 통신
	-  MSA 구조에서의 통신
		- MSA 구조에서 서비스가 결합 및 의존도를 낮춤
- 오래 걸리는 프로세스나 백그라운드로 실행이 필요한 경우

### 2. 사례
![Pasted image 20230209082127.png](./assets/Pasted image 20230209082127.png)
![Pasted image 20230209082134.png](./assets/Pasted image 20230209082134.png)
![Pasted image 20230209082201.png](./assets/Pasted image 20230209082201.png)

## MQ의 기본 구조 ✔

> SQS 예시

![Pasted image 20230208082243.png](./assets/Pasted image 20230208082243.png)


## SQS 의 기본 개념 ✔

#### 메시지
- SQS 기본 단위
- 메시지는 XML, JSON 등과 같은 텍스트 형태이며 최대 **64KB**까지 보낼 수 있음
	- 3~4 KB 크기의 메시지라도 64KB로 책정되기 때문에, 배치API로 최대한 메시지를 모아서 처리하는 게 좋음
- 보관 기간을 초 단위로, 1분부터 14일 까지 설정할 수 있음

#### 큐
- 메시지를 담는 공간
- 담을 수 있는 메시지 개수는 무제한
- region 내, region 간 모두 메시지를 주고 받을 수 있음
	- 같은 region 내에 큐나 EC2 인스턴스 끼리 메시지 주고 받는 것은 무료
- **30일 내에 요청이 없으면 AWS 큐가 삭제 될 수 있으므로 설계시 고려**

#### 배치 API
- 한 번에 메시지를 최대 10개 또는 최대 256KB까지 동시 처리 가능
- 메시지를 여러 개 합쳐서 64KB 이하일 때 요청 1개로 청구됨

#### 보기 제한 시간
- 메시지를 받은 뒤 특정 시간 동안 다른 곳에서 동일한 메시지를 다시 꺼내볼 수 없게 하는 기능
	- 큐 하나에 여러 서버가 메시지를 받을 때 동일한 메시지의 중복 처리 방지
- 0~12초까지 설정

#### 지연 전송 ❗️
- 특정 시간 동안 메시지를 받지 못하게 하는 기능
- 지연되는 시간 동안에는 Messages in Flight에 포함됨

#### 처리 실패 큐
- 일반적으로 처리되면 메시지를 삭제함, 그러나 메시지가 실패할 경우 처리 실패 큐로 보냄
- 처리 실패 큐는 일반 큐와 같은 region에 생성해야 함

#### 폴링

##### 짧은 폴링
- 메세지 받기 요청을 하면 바로 결과를 받음
	- ReceiveMessage 요청에서 WaitTimeSeconds를 0으로 설정했을 때
	- 큐 설정의 ReceiveMessageWaitTimeSeconds를 0으로 설정했을 때

##### 긴 폴링
- 메시지가 있으면 바로 가져오고, 없으면 올 때까지 기다림, 오지 않으면 폴링 시간까지 기다림


## SQS 함수 종류 

-   CreateQueue : 큐 생성
-   ListQueues : 큐 목록 출력
-   DeleteQueue : 큐 삭제
-   SendMessage : 큐에 메세지 추가
-   SendMessageBatch : 큐에 여러 메세지 추가
-   ReceiveMessage : 큐에서 메세지를 꺼내서 보기
-   ChangeMessageVisibility : 메세지의 보기 제한 시간 변경
-   ChangeMessageVisibilityBatch : 여러 메세지의 보기 제한 시간 변경
-   DeleteMessage : 큐에서 메세지 삭제
-   DeleteMesasgeBatch : 큐에서 여러 메세지 삭제
-   SetQueueAttributes : 큐 설정 변경 (지연 전송 시간, 최대 메세지 크기, 메세지 보관 기간, 접근 정책, 짧은 폴링/긴 폴링 시간, 처리 실패 큐)
-   GetQueueAttributes : 큐 설정 확인 (메세지 개수, 큐 생성 시간, 최종 큐 변경 시간, 큐 ARN, 지연 전송 시간, 최대 메세지 크기, 메세지 보관 기간, 접근 정책, 짧은 폴링/긴 폴링 시간, 처리 실패 큐)
-   GetQueueUrl : 큐 엔드포인트 URL 확인
-   AddPermission : 다른 AWS 계정에 대한 접근 권한 설정
-   RemovePermission : 다른 AWS 계정에 대한 접근 권한 삭제


----

## AWS SQS 및 Lambda 생성 ✔

### 1. AWS SQS Console 에서 페이지 접근 및 대기열 생성을 누른다
![Pasted image 20230208082404.png](./assets/Pasted image 20230208082404.png)

### 2. 관련 옵션을 선택한다

#### 2-1 Queue 유형을 선택

> **표준** : 속도가 빠른 대신 중복 처리, 순서성 보장 X
> FIFO : 선입선출 및 중복 처리 보장, 단점으로 300 TPS 까지 처리

![Pasted image 20230208082513.png](./assets/Pasted image 20230208082513.png)

#### 2-2 메시지에 따른 세부옵션을 선택 후 생성

> **표시 제한 시간** : 특정 Consumer가  메시지를 읽었을 때, 다른 Consumer 들에게 보이지 않는 시간
> 설정, 표시 제한 시간 내에 메시지 처리 및 삭제가 이뤄져야함. 그렇지 않으면 중복이 발생


![Pasted image 20230208083651.png](./assets/Pasted image 20230208083651.png)

##### Message Life Cycle
![Pasted image 20230208084156.png](./assets/Pasted image 20230208084156.png)


#### 2-3 Lambda Console 로 이동 후 함수 생성 버튼 누름
![Pasted image 20230208084925.png](./assets/Pasted image 20230208084925.png)

#### 2-4 Lambda 함수 이름 및 런타임 언어를 선택
![Pasted image 20230208085124.png](./assets/Pasted image 20230208085124.png)

### 3. Lambda 생성 및 해당 함수 test 실행 후 확인

> 함수 작성 후 저장(Ctrl + s) 및 Deploy 해야 Test 시 코드 반영

#### 함수 작성

![Pasted image 20230208092258.png](./assets/Pasted image 20230208092258.png)

#### Test

![Pasted image 20230208092317.png](./assets/Pasted image 20230208092317.png)

### 4. Lambda에서 SQS 를 호출할 수 있도록 권한 설정


#### 4-1 함수 내의 구성 > 권한 > 실행 역할의 편집 버튼 

![Pasted image 20230208094212.png](./assets/Pasted image 20230208094212.png)

#### 4-2 역할 설정 및 저장
> IAM에서 권한을 `AmazonSQSFullAccess` 를 포함하여 정책을 미리 생성해두는 것이 좋음

![Pasted image 20230208095238.png](./assets/Pasted image 20230208095238.png)


### 5. SQS 트리거 추가

#### 트리거 추가 버튼 클릭
![Pasted image 20230208100525.png](./assets/Pasted image 20230208100525.png)

#### 미리 생성해둔 SQS 지정 및 추가

![Pasted image 20230208100652.png](./assets/Pasted image 20230208100652.png)

#### 구성 > 트리거에 들어가보면 생성된 트리거 확인가능

![Pasted image 20230208100753.png](./assets/Pasted image 20230208100753.png)

#### 이 후 함수 작성

